#include <stdio.h>
#include <string.h> //for strlen
#include <stdlib.h> //for exit
#include "md5.h" //for hash function

int main(int argc, char*argv[])
{
    if (argc<3)
    {
        printf("Two filenames are required.\n");
        exit(1);
    }
    
    //open password file for reading
    FILE *f1 = fopen(argv[1], "r");
    if (!f1)
    {
        fprintf( stderr, "Cannot open %s for reading\n", argv[1]);
        exit(2);
    }
    
    //open second file to write hashes
    FILE *f2 = fopen(argv[2], "w");
    if (!f2)
    {
        fprintf(stderr, "Cannot open %s for writing\n", argv[2]);
        exit(3);
    }
    
    //char array to hold lines in file
    char line[100];
    //an array of strings to hold hashes
    const char *h[100];
    while ( fgets(line, 100, f1) != NULL )
    {
        for ( int i = 0; i < strlen(line); i++)
        {
            //pass passwords into md5 function with their lengths
            h[i] = md5(&line[i], strlen(&line[i]));
            //fprintf(f2, "%s\n", h[i]);
        }
        
        fprintf(f2, "%s\n", *h);
        //for(int j = 0; h[j] != '\0'; j++)


    }
}
